#include <iostream>
#include <fstream>
#include <time.h>
#include "Fibonacci.hpp"

#define DATAPOINTS 200

void createDatasetEraseSerial();

int main(int argc, char* argv[]) {

	createDatasetEraseSerial();

}

void createDatasetEraseSerial() {
	clock_t start, end;
	FibHeap fib = FibHeap();
	std::ofstream output;
	output.open("erase_serial_dataset.dat");
	output << "# Size of tree		Time to erase" << std::endl;
	
	for (int t = 0; t < DATAPOINTS; t++) {
		fib = FibHeap();
		for (int i = 0; i < t; i++) {
			fib.insert(i + 1);
		}

		//Delete all the nodes and time accordingly
		for (int i = 0; i < t; i++) {
			start = clock();
			fib.erase_min();
			end = clock();

			output << fib.size() << "				" << (double)(end - start) << std::endl;
		}
	}
	

	output.close();
}