#ifndef __FIBONACCI_C
#define __FIBONACCI_C

#include <iostream>
#include <math.h>
#include <vector>
#include "Fibonacci.hpp"

void FibHeap::insert(const int elem) {

	Node* tmp = insert_unsafe(elem);
	
}

FibHeap::Node* FibHeap::insert_unsafe(const int elem) {
	
	//Create a new tree with a single node (to use merge later).
	FibHeap newTree = FibHeap(elem);

	Node* res = newTree._min;
	//Merge the trees
	merge(&newTree);

	return res;
}

void FibHeap::merge(FibHeap *other) {
	if (_size == 0) { //Our tree is empty
		_rightmost = other->_rightmost;
		_leftmost = other->_leftmost;
		_min = other->_min;
	}
	else {
		/*
		Link the near end
		What before was
		_____________		_____________
		|			v		|			v
		(lft)===(rght)		(lft)===(rght)
		^			|		^			|
		-------------		-------------

		Now becomes
		_____________
		|			v
		(lft)===(rght)=====(lft)===(rght)
		^			|
		-------------
		*/
		other->_rightmost->_right = _leftmost;
		_leftmost->_left = other->_rightmost;

		/*
		Link the near end
		What before was
		_____________
		|			v
		(lft)===(rght)=====(lft)===(rght)
		^			|
		-------------
		Now becomes
		________________________________
		|								v
		(lft)===(rght)=====(lft)===(rght)
		^								|
		---------------------------------
		*/

		other->_leftmost->_left = _rightmost;
		_rightmost->_right = other->_leftmost;

		//Update rightmost and leftmost
		_leftmost = other->_leftmost;
		//Update min
		if (other->_min->_elem < _min->_elem) {
			_min = other->_min;
		}
	}

	//Update size
	_size += other->_size;
}

void FibHeap::print() {
	Node* curTop = _leftmost; //To traverse the top list

	std::cout << "| ";
	while (curTop->_left != _leftmost) {
		std::cout << curTop->_elem << " | ";
		curTop = curTop->_left;
	}

	std::cout << curTop->_elem << " | " << std::endl;
	
}

const int FibHeap::find_min() { //Retrieve the minimum elemnt (no delete)
	if (_min != nullptr) {
		return _min->_elem;
	}
	else {
		return -1;
	}
}

const int FibHeap::size() {
	return _size;
}

void FibHeap::erase_min() {
	Node* m = _min;

	if (m != nullptr) {
		//Put children in the root list
		if (m->_son != nullptr) {
			Node* cur = m->_son;
			Node* next = m->_son->_right;
			do{
				cur = next;
				next = next->_right;
				addToRoot(cur);
			} while (cur != m->_son);
		}
		//Delete min rom root list
		deleteFromRoot(_min);

		//Assign new Min
		if (m->_right == m) {
			if (_leftmost != nullptr && _rightmost != nullptr) {
				_min = nullptr;
				consolidate();
			}
		}
		else {
			_min = m->_right;
			consolidate();
		}
		delete m;
		_size--;
	}	
}

void FibHeap::decrease_key(Node* n, int new_key) {
	if (new_key >= n->_elem) {
		//Error: new_key has to be smaller than the current key
	}
	else {
		n->_elem = new_key;
		Node* parent = n->_parent;
		if (parent != nullptr && n->_elem < parent->_elem) { //We have to reorder
			cut(n, parent);
			cascading_cut(parent);
		}
		if (n->_elem < _min->_elem) {
			_min = n;
		}
	}
}

void FibHeap::delete_node(Node* n) {
	decrease_key(n, -1);
	erase_min();
}


/* Private Methods */

void FibHeap::consolidate() {
	//Initialize the node array
	int arraysize = (int)log2((double)_size) + 1;	
	std::vector<FibHeap::Node*> nodes = std::vector<Node*>(arraysize);

	for (int i = 0; i < arraysize; i++) {
		nodes[i] = nullptr;
	}

	//For each node in the root list
	Node *current = _leftmost;
	Node *next = current->_left;
	do {
		//Jump to next node
		current = next;
		next = next->_left;
		//If the current position of the aray is occupied, consolidate
		while (nodes[current->_deg] != nullptr && nodes[current->_deg] != current) {
			Node* other = nodes[current->_deg];
			if (other->_elem < current->_elem) {
				
				//Exchange other and current
				Node *temp = other;
				other = current;
				current = temp;
				//nodes[current->_deg] = other;
				
			}
			//Free that array position
			nodes[current->_deg] = nullptr;
			//Add both to the tree as a new subtree
			link(current, other);			
		}
		//Occupy the position
		nodes[current->_deg] = current;	
		
	} while (current != _leftmost);

	//Recalculate the minimum and add to root list
	_min = nullptr;
	for (int i = 0; i < arraysize; i++) {
		if (nodes[i] != nullptr) {
			//addToRoot(nodes[i]);
			if (_min == nullptr || _min->_elem > nodes[i]->_elem) {
				_min = nodes[i];
			}
		}
	}
}

//Hang y from x
void FibHeap::link(Node* x, Node* y) {
	
	//Sanitize _leftmost and _rightmost
	if (_leftmost == y) {
		_leftmost = x;
	}

	if (_rightmost == y) {
		_rightmost = x;
	}

	//Remove y from the root list
	/*	
	What before was
	
	(y->_left)====(y)====(y->_right)
	
	Now becomes

	(y) (y->_left)====(y->_right)
	*/
	deleteFromRoot(y);
	//y->_left->_right = y->_right;
	//y->_right->_left = y->_left;

	//Make y a child of x
	y->_parent = x;
	if (x->_son != nullptr) {
		//Concatenate y to the list of children of x, from the left
		Node* curLeft = x->_son->_left;
		x->_son->_left = y;
		y->_left = curLeft;
		y->_right = x->_son;
		curLeft->_right = y;
	}
	else {
		x->_son = y;
		y->_right = y;
		y->_left = y;
	}
	y->_mark = false;
	x->_deg++;
}

void FibHeap::addToRoot(Node* n) {
	//We insert it at the left of _min
	Node* curLeft = _min->_left;
	_min->_left = n;
	n->_left = curLeft;
	n->_right = _min;
	curLeft->_right = n;

	if (_min == _leftmost) {
		_leftmost = n;
	}

	n->_parent = nullptr;
	n->_mark = false;
}

void FibHeap::deleteFromRoot(Node* n) {
	if (n->_right == n && n->_left == n && n->_son == nullptr) {
		//It's the only node on the root
		_min = nullptr;
		_leftmost = nullptr;
		_rightmost = nullptr;
	}
	else {
		if (n == _leftmost) {
			_leftmost = n->_right;
		}

		if (n == _rightmost) {
			_rightmost = n->_left;
		}
		Node* l = n->_left;
		//Restructure the list
		n->_left->_right = n->_right;
		n->_right->_left = l;

		//Isolate the node
		//n->_left = n;
		//n->_right = n;
	}
}

void FibHeap::cut(Node* child, Node* parent) {
	parent->_deg--;
	if (parent->_deg != 0) {
		if (parent->_son == child) { //It's not the only son
		parent->_son = child->_right;
		}
	}
	else {
		parent->_son = nullptr;
	}
	addToRoot(child);
}

void FibHeap::cascading_cut(Node* n) {
	Node* parent = n->_parent;
	if (parent != nullptr) {
		if (!n->_mark) {
			n->_mark = true;
		}
		else {
			cut(n, parent);
			cascading_cut(parent);
		}
	}
}

#endif