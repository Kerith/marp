#ifndef __FIBONACCI_H
#define __FIBONACCI_H


//template <class T>
class FibHeap {
public: /* Constructors */
	
	//Default constructor
	FibHeap() :
		_min(nullptr),
		_leftmost(nullptr),
		_rightmost(nullptr),
		_size(0) {} 
	
	//Constructor for a single-node tree
	FibHeap(int elem)  {
		_min = new Node(elem);
		_rightmost = _min;
		_leftmost = _min;
		_size = 1;
	}

	~FibHeap() {

	}

//protected:
public:
	//template <class T>
	class Node {
	public: /* Constructors */

		Node() : //Default constructor
			_elem(0),
			_left(this),
			_right(this),
			_parent(nullptr),
			_son(nullptr),
			_mark(false),
			_deg(0) {}

		Node(int elem) : //Unconnected node
			_elem(elem),
			_left(this),
			_right(this),
			_parent(nullptr),
			_son(nullptr),
			_mark(false),
			_deg(0) {}

		~Node() { //Destructor
			_left = nullptr;
			_right = nullptr;
			_parent = nullptr;
			_son = nullptr;
		}

	 /* Fields */

		//Actual values
		int _elem;

		//Pointers for the linked lists
		Node* _left;
		Node* _right;
		Node* _parent;
		Node* _son;

		//Mark. True if it has been cut, false otherwise.
		bool _mark;

		//Degree of the node (as in Binomial Heaps).
		int _deg;
	
	};

private: /* Fields */

	Node *_min;
	Node *_leftmost;
	Node *_rightmost;

	int _size; //Number of nodes in the tree

public: /* Public Methods */

	void insert(const int elem); //Insert an element into the tree
	Node* insert_unsafe(const int elem);
	void merge(FibHeap *other); //Merge "other" with this from the right: (other)---(this)
	const int find_min(); //Retrieve the minimum elemnt (no delete)
	void erase_min(); //Erase the minimum
	void decrease_key(Node *n, int new_key); //Decrease the key of a node by 1
	void delete_node(Node* n);
	const int size(); //The size of the tree

	/* Auxiliary Functions */
	void print();

private: /* Private Methods */

	void consolidate();
	void link(Node* lhs, Node* rhs);
	void addToRoot(Node* node);
	void deleteFromRoot(Node* node);
	void cut(Node* child, Node* parent);
	void cascading_cut(Node* parent);

};

#endif