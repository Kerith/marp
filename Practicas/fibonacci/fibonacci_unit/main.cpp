#include <iostream>
#include <gtest/gtest.h>
#include <vector>
#include "Fibonacci.cpp"

int main(int ac, char* av[]) {
	
	testing::InitGoogleTest(&ac, av);
	return RUN_ALL_TESTS();

}

TEST(StructuralTest, NodeInsertionSafeNoInternal) {
	FibHeap *fib = new FibHeap(3);
	EXPECT_EQ(3, fib->find_min());
	fib->insert(4);
	EXPECT_EQ(3, fib->find_min());
	fib->insert(1);
	EXPECT_EQ(1, fib->find_min());
}

TEST(StructuralTest, NodeInsertionUnsafe) {
	FibHeap fib = FibHeap();
	std::vector<FibHeap::Node*> nodes = std::vector<FibHeap::Node*>();
	nodes.push_back(fib.insert_unsafe(1));
	nodes.push_back(fib.insert_unsafe(2));
	nodes.push_back(fib.insert_unsafe(3));
	nodes.push_back(fib.insert_unsafe(4));
	nodes.push_back(fib.insert_unsafe(5));

	FibHeap::Node* curNode = nodes.front();
	//Check to the right
	for (int i = 0; i < nodes.size(); i++) {
		EXPECT_EQ(curNode->_elem, nodes.at(i)->_elem);
		EXPECT_EQ(nodes.at(i)->_parent, nullptr);
		EXPECT_EQ(nodes.at(i)->_son, nullptr);
		curNode = curNode->_left;
	}
	//Check to the left
	curNode = curNode->_right;
	for (int i = nodes.size() - 1; i >= 0; i--) {
		EXPECT_EQ(curNode->_elem, nodes.at(i)->_elem);
		EXPECT_EQ(nodes.at(i)->_parent, nullptr);
		EXPECT_EQ(nodes.at(i)->_son, nullptr);
		curNode = curNode->_right;
	}
}

TEST(StructuralTest, TreeMergeSize) {
	FibHeap *fib = new FibHeap(3);
	fib->insert(4);
	fib->insert(1);
	EXPECT_EQ(3, fib->size());

	FibHeap *other = new FibHeap(4);	
	other->insert(9);
	other->insert(6);
	EXPECT_EQ(3, other->size());

	fib->merge(other);
	EXPECT_EQ(6, fib->size());

}

TEST(StructuralTest, EraseMin) {
	FibHeap fib = FibHeap(1);
	
	fib.insert(2);
	fib.insert(3);
	fib.insert(4);
	fib.insert(5);

	fib.erase_min();
	EXPECT_EQ(2, fib.find_min());
	fib.erase_min();
	EXPECT_EQ(3, fib.find_min());
	fib.erase_min();
	EXPECT_EQ(4, fib.find_min());
	fib.erase_min();
	EXPECT_EQ(5, fib.find_min());
	fib.erase_min();
	EXPECT_EQ(-1, fib.find_min());

}

TEST(StructuralTest, DecreaseKeySimple) {
	FibHeap fib = FibHeap();
	std::vector<FibHeap::Node*> nodes = std::vector<FibHeap::Node*>();
	fib.insert(1);
	nodes.push_back(fib.insert_unsafe(2));
	nodes.push_back(fib.insert_unsafe(3));

	fib.erase_min();

	FibHeap::Node* curNode = nodes.front();

	EXPECT_EQ(curNode->_elem, fib.find_min());
	//Check the structural integrity of the tree
	EXPECT_EQ(fib.find_min(), 2);
	EXPECT_EQ(curNode->_parent, nullptr);
	EXPECT_EQ(curNode->_son->_elem, 3);
	EXPECT_EQ(curNode->_deg, 1);
	//Decrease key. Now the tree should be (1)===(2)
	fib.decrease_key(curNode->_son, 1);

	EXPECT_EQ(curNode->_son, nullptr);
	EXPECT_EQ(curNode->_right->_elem, 1);
	EXPECT_EQ(curNode->_mark, false);
	EXPECT_EQ(curNode->_right->_right, curNode);
	EXPECT_EQ(fib.find_min(), 1);
	EXPECT_EQ(curNode->_deg, 0);

}

TEST(StructuralTest, DecreaseKeySon) {
	FibHeap fib = FibHeap();
	std::vector<FibHeap::Node*> nodes = std::vector<FibHeap::Node*>();
	fib.insert(1);
	nodes.push_back(fib.insert_unsafe(2));
	nodes.push_back(fib.insert_unsafe(3));
	nodes.push_back(fib.insert_unsafe(4));
	nodes.push_back(fib.insert_unsafe(5));

	fib.erase_min();

	FibHeap::Node* curNode = nodes.front();

	EXPECT_EQ(curNode->_elem, 2);
	EXPECT_EQ(curNode->_son->_elem, 3);
	EXPECT_EQ(curNode->_deg, 2);
	fib.decrease_key(curNode->_son, 1);

	//Check for structural integrity
	EXPECT_EQ(curNode->_deg, 1);
	EXPECT_EQ(curNode->_son->_elem, 4);
	EXPECT_EQ(curNode->_son->_deg, 1);
	EXPECT_EQ(curNode->_son->_parent, curNode);
	EXPECT_EQ(curNode->_mark, false);

	EXPECT_EQ(curNode->_right->_elem, 1);
	EXPECT_EQ(curNode->_right->_parent, nullptr);
	EXPECT_EQ(fib.find_min(), 1);

}

TEST(StructuralTest, DecreaseKeyCascade) {
	FibHeap fib = FibHeap();
	std::vector<FibHeap::Node*> nodes = std::vector<FibHeap::Node*>();
	fib.insert(1);
	nodes.push_back(fib.insert_unsafe(2));
	nodes.push_back(fib.insert_unsafe(3));
	nodes.push_back(fib.insert_unsafe(4));
	nodes.push_back(fib.insert_unsafe(5));

	fib.erase_min();

	FibHeap::Node* curNode = nodes.front();

	EXPECT_EQ(curNode->_elem, 2);
	EXPECT_EQ(curNode->_son->_elem, 3);
	EXPECT_EQ(curNode->_deg, 2);
	fib.decrease_key(curNode->_son->_right, 1);

	//Check for structural integrity
	EXPECT_EQ(curNode->_deg, 1);
	EXPECT_EQ(curNode->_son->_elem, 3);
	EXPECT_EQ(curNode->_son->_deg, 0);
	EXPECT_EQ(curNode->_son->_parent, curNode);
	EXPECT_EQ(curNode->_mark, false);

	EXPECT_EQ(curNode->_right->_elem, 1);
	EXPECT_EQ(curNode->_right->_parent, nullptr);
	EXPECT_EQ(curNode->_right->_deg, 1);
	EXPECT_EQ(curNode->_right->_son->_elem, 5);
	EXPECT_EQ(fib.find_min(), 1);

}

